﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Testi
{
   public class Tuotteet
    {
        string Tuote;
        int Määrä;
        double Hinta;

        public string tuote { get => Tuote; set => Tuote = value; }
        public double hinta { get => Hinta; set => Hinta = value; }
        public int määrä { get => Määrä; set => Määrä = value; }

        public string tulosta()
        {
            return tuote + ": "+ hinta + "e";
        }
        public string Yhteishinta()
        {
            return tuote + ": "+ määrä + "kpl, " + (hinta * määrä) + "e ";
        }
        public string Ostoskori()
        {
            return tuote + ": " + määrä + "kpl, " + (hinta * määrä) + "e ";
        }
    }
}
